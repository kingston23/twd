#include <sys/time.h>
#include <iostream>
#include <string>
#include "ros/ros.h"

//#include <algorithm>

//#include <twdplanner/Point.h>
#include "geometry_msgs/Point.h"

#include <twdplanner/Path.h>
#include <pathtwdlistener/FilteredPath.h>
#include <pathtwdlistener/FixOrientation.h>
#include <pathtwdlistener/patrolingGoals.h>


#include "moj.h"   //DStar.h, Params.h, Planner.h


struct T_point{
	double  x, y;
};

  T_point tr;
  std::vector<T_point> lastfilpath;
  std::vector<T_point> patrollingpath;
class FilteredPointsTWD
{
protected:
  // Our NodeHandle
  ros::NodeHandle nh_;
  tf::TransformListener tf_listener;

public:
  double robottfx, robottfy, robottfth;
  double alfa, beta, d2, theta;
  double velx,vely, velz;

  int filtered = 0;
  int twdfiltered = 0;
  int fixed = 0;
  int newSegment = 0;
  int amcl_pose_read = 0;
  int firstTWD = 0;
  //double newPoint = 0.255; // udaljenost na kojoj se ubacuje nova tocka
  double newPoint = 1.1;
  double poseRot = 0;
  double golden = 1.618; //golden ratio = 1.618, silver ratio = 2.4142, bronze ratio = 3.30277
  double du;

  double lasertfx2;//laser coordinates (global)
  double lasertfy2;
  double lasertfth2;

	ros::Publisher filteredpath_pub;
  ros::Publisher fixorietnation_pub;
	ros::Subscriber pathtwd_sub;
  ros::Subscriber patrollingpath_sub;
  ros::Subscriber amcl_pose_sub;
  ros::Subscriber fix_filteredpath_sub;
  ros::Subscriber laser_sub;
  ros::Subscriber vel_sub;
  ros::Subscriber odomsub;

  pathtwdlistener::FilteredPath filpath;
  pathtwdlistener::FilteredPath twdfilpath;

  pathtwdlistener::FixOrientation fixpath;

  

  FilteredPointsTWD(ros::NodeHandle n) :
      nh_(n)
  {
 	pathtwd_sub = nh_.subscribe("twdPath",1,&FilteredPointsTWD::FilteredPathCallback, this); //TWD

	filteredpath_pub = nh_.advertise<pathtwdlistener::FilteredPath>("filtered_path",10);

  fix_filteredpath_sub = nh_.subscribe("filtered_path",1,&FilteredPointsTWD::FixPathCallback, this);
  fixorietnation_pub = nh_.advertise<pathtwdlistener::FixOrientation>("fix_path",10);
  //amcl_pose_sub = nh_.subscribe("/amcl_pose", 1, &FilteredPointsTWD::globalposeCallback, this); 
  odomsub = nh_.subscribe("/odom", 10, &FilteredPointsTWD::odomCallback, this); // za patroling primjer DUV
  //odomsub = nh_.subscribe("/steer_bot/ackermann_steering_controller/odom", 10, &FilteredPointsTWD::odomCallback, this); // za ackermann

  laser_sub = nh_.subscribe("/scan", 1, &FilteredPointsTWD::laserRearCallback, this); 
  //laser_sub = nh_.subscribe("/steer_bot/scan", 1, &FilteredPointsTWD::laserRearCallback, this); // za ackermann


  vel_sub=nh_.subscribe<geometry_msgs::Twist>("/cmd_vel", 1, &FilteredPointsTWD::velCallback, this);
  //vel_sub=nh_.subscribe<geometry_msgs::Twist>("/steer_bot/ackermann_steering_controller/cmd_vel", 1, &FilteredPointsTWD::velCallback, this); // za ackermann


  }
  void FilteredPathCallback( const twdplanner::PathConstPtr& pMsg);   //TWD

  void FixPathCallback( const pathtwdlistener::FilteredPathConstPtr& pMsg);
  void globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
  void laserRearCallback(const sensor_msgs::LaserScan::ConstPtr& msg);
  void velCallback(const geometry_msgs::Twist::ConstPtr& msg);
  void odomCallback(const nav_msgs::Odometry::ConstPtr& msg);


};


int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "pathtwdlistener");
  ros::NodeHandle nh;
  FilteredPointsTWD filteredTWD(nh);

  lastfilpath.reserve(10000); 
  lastfilpath.clear();
  patrollingpath.reserve(10000); 
  patrollingpath.clear();
  ros::Rate rate(10.0);

  while (nh.ok()) {
    ros::spinOnce(); 
    
//	  filteredTWD.publishnewfilteredmsg();	
   
	  rate.sleep();
  }
  return 0;
}

int ispis = 0;
void FilteredPointsTWD::globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
  robottfx=msg->pose.pose.position.x;
 	robottfy=msg->pose.pose.position.y;
 	// robottfth=tf::getYaw(msg->pose.pose.orientation);
  // if (ispis==0){
	//   ROS_INFO("\n First postion and orientation: x [%f], y [%f] and th [%f] deg\n", robottfx, robottfy, robottfth*180/M_PI);
  //   ispis=1;
  // }
}

void FilteredPointsTWD::odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  //ROS_INFO("Seq: [%d]", msg->header.seq);
  //ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
  //ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
  //ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", msg->twist.twist.linear.x,msg->twist.twist.angular.z);
  //odom_v=msg->twist.twist.linear.x;
  //odom_w=msg->twist.twist.angular.z;
  robottfth = tf::getYaw(msg->pose.pose.orientation);
  }

void FilteredPointsTWD::laserRearCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
  
  	for(int i_LS=350;i_LS<370;i_LS++){
      if(msg->ranges[i_LS] < newPoint)
      {
        //printf("laser rages from %d is %f \n",i_LS, msg->ranges[i_LS]);
        poseRot = 1; //postavi zastavicu da je prepreka ispred robota

      }else{
        //printf("laser rages from %d is %f \n",i_LS, msg->ranges[i_LS]);
        poseRot = 0; 
      }

    }
}

void FilteredPointsTWD::velCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
  velx = msg -> linear.x;
 	vely = msg -> linear.y;
 	velz = msg -> linear.z;
	
  if(abs(velx) < 0.02 ){
    fixed = 0;
    newSegment = 0;
    //firstTWD = 0; // restartaj TWD*

  }
 }

void FilteredPointsTWD::FilteredPathCallback(const twdplanner::PathConstPtr& pMsg) //TWD
{
  geometry_msgs::Point p; 
  twdfilpath.points.clear();
  ROS_INFO("%d tocaka nakon twd: ", pMsg->points.size());
  // for(int i = 0; i< pMsg->points.size(); i++){
  //     ROS_INFO("%d tocaka nakon twd: x: %f, y: %f", i, pMsg->points[i].x, pMsg->points[i].y);
  // }
      p.x=pMsg->points[0].x;
      p.y=pMsg->points[0].y;
  twdfilpath.points.push_back(p);

  beta = atan2(pMsg->points[1].y-pMsg->points[0].y, pMsg->points[1].x-pMsg->points[0].x);   //kut prvog segmeta

  // izbaivanje tocaka i poravnanje orijetacije robota sa segmentom
  for (int i=1; i<pMsg->points.size()-1; i++){
    alfa = atan2(pMsg->points[i+1].y-pMsg->points[i].y, pMsg->points[i+1].x-pMsg->points[i].x);   //atan2(y,x)

    if (abs(alfa) > (abs(beta)-0.00001) && abs(alfa) < (abs(beta)+0.00001)){
      p.x=pMsg->points[i].x;
      p.y=pMsg->points[i].y;
      //ROS_INFO("Filtiranje: duplic na putu %d: [%f %f %f %f]", i, p.x, p.y, alfa, beta);
    }else{
      p.x=pMsg->points[i].x;
      p.y=pMsg->points[i].y;
      //ROS_INFO("Tocka na putu koja nije duplic %d: [%f %f %.15f %.15f]", i, p.x, p.y, alfa, beta);
      twdfilpath.points.push_back(p);
      beta = alfa;
      newSegment = 0;
    }
  }
  int end = pMsg->points.size();
      p.x=pMsg->points[end-1].x;
      p.y=pMsg->points[end-1].y;
    //  ROS_INFO("Filtiranje: zadnja tocka na putu: [%f %f]", p.x, p.y);
      twdfilpath.points.push_back(p);

  ROS_INFO("Nakon filtiranja imamo broj tocaka: %d", twdfilpath.points.size());
  ROS_INFO("twdfilpath %d, lastfilpath %d", twdfilpath.points.size(), lastfilpath.size());
  for (int i=1; i<twdfilpath.points.size(); i++ ){
    ROS_INFO("%f %f %f %f", lastfilpath[i].x,lastfilpath[i].y, twdfilpath.points[i].x, twdfilpath.points[i].y); //provjera ako je doslo do replaniranja twd puta
    if (lastfilpath[i].x == twdfilpath.points[i].x && lastfilpath[i].y == twdfilpath.points[i].y){
      twdfiltered = 1;
    }else{
      ROS_INFO("new TWD path - REPLANNING");
      twdfiltered = 0;
      break;
    }  
  }

  if(twdfiltered == 0){
    lastfilpath.clear();
    for (int i=0; i<twdfilpath.points.size(); i++ ){
      tr.x = twdfilpath.points[i].x;
      tr.y = twdfilpath.points[i].y;
      lastfilpath.push_back(tr);
    ROS_INFO("FILTERED TWD* PATH: point on the path %d: [%f %f]", i, tr.x, tr.y);
    }

    //ROS_INFO("Publish filered twd path and other patrolling goals.");
    // if (firstTWD == 0){
    //   filteredpath_pub.publish(twdfilpath);  //objavljivanje poruke na temu 
    //   firstTWD = 1;
    //   for (int i=0; i<twdfilpath.points.size(); i++ ){
    //     tr.x = twdfilpath.points[i].x;
    //     tr.y = twdfilpath.points[i].y;
    //     ROS_INFO("Publish filered twd path and other patrolling goals: %f, %f", tr.x, tr.y);
    //   }
    // }
      ROS_INFO("PPPPPPPPPPPPPPPPPPPPPPPPUUUUUUUUUUUUUUUUUUUUUBBB");
    filteredpath_pub.publish(twdfilpath);  //objavljivanje poruke na temu 
  }
  
  //filtered = 0;
}

void FilteredPointsTWD::FixPathCallback(const pathtwdlistener::FilteredPathConstPtr& pMsg)
{
  if( remove( "goal_position.txt" ) != 0 ){ //potrebno brisanje datoteke - ako se u prvom pozivu spremi vise tocaka nego u drugom, ostale tocke se gledaju kao ciljevi
    printf( "Error deleting file \n" );
  }
  else{
    printf( "File goal_position.txt successfully deleted! \n" );
  }
  //citanje senzora. ako se napazi prepreka tocno ispred robota nek ipak rotira na mjestu, inace popravi kut
  geometry_msgs::Point p; 
  fixpath.points.clear();
  //ROS_INFO("CISTIMO SVEEEEEE i printamo broj tocaka za popravak orijentacije: %d", pMsg->points.size());
  if(firstTWD==1){
  beta = atan2(pMsg->points[1].y-pMsg->points[0].y, pMsg->points[1].x-pMsg->points[0].x);   //kut prvog segmeta
  if (abs(beta)<0.02){
    beta = 0;
  }

  ROS_INFO("Orijentacija prvog segmenta: %f deg, %f rad", beta*180/M_PI, beta);

  p.x=pMsg->points[0].x;
  p.y=pMsg->points[0].y;
  //ROS_INFO("Ubacujem tocku na putu robot orijentiran za 90°: [%f %f]", p.x, p.y);
  fixpath.points.push_back(p); 

  // poravnanje orijetacije robota sa segmentom
   for (int i=1; i<pMsg->points.size()-1; i++){
   //orijentacije su poravnete dodaj pocetnu tocku kao start
      p.x = pMsg->points[i].x;
      p.y = pMsg->points[i].y;
      //ROS_INFO("Dodajem tocku na put. %d segment: [%f %f]", i,p.x, p.y);
      fixpath.points.push_back(p);
    }
  
   
  int end = pMsg->points.size();
  p.x=pMsg->points[end-1].x;
  p.y=pMsg->points[end-1].y;
  ROS_INFO("Zadnja tocka na putu: [%f %f]", p.x, p.y);
  fixpath.points.push_back(p);
    fixed++;
  }
  else{
       for (int i=0; i<pMsg->points.size(); i++){
        p.x=pMsg->points[i].x;
        p.y=pMsg->points[i].y;
        fixpath.points.push_back(p);
  }
  fixorietnation_pub.publish(fixpath);  //objavljivanje poruke na temu 
  
//  filtered = 0;
}
   ROS_INFO("saljemo fix orijentacije s %d tocaka", fixpath.points.size());
  fixorietnation_pub.publish(fixpath);  //objavljivanje poruke na temu - dodano za patrolling

  //spremanje tocaka u datoteku
  FILE	*goallogfile;
  if ( (goallogfile = fopen("goal_position.txt","w")) == NULL ){
	  printf("Error! Input file couldn't be opened.");
	}else{
    for (int i=0; i < fixpath.points.size(); i++ ){
      p.x = fixpath.points[i].x;
      p.y = fixpath.points[i].y;
		  fprintf(goallogfile,"%f %f \n",p.x,p.y);
    }
	}
	if (goallogfile!=NULL){
			  fclose(goallogfile);
	}
  
}


