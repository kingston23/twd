#include <sys/time.h>
#include <iostream>
#include <string>
#include "ros/ros.h"

//#include <algorithm>

//#include <twdplanner/Point.h>
#include "geometry_msgs/Point.h"

#include <twdplanner/Path.h>
#include <pathtwdlistener/FilteredPath.h>


#include "moj.h"   //DStar.h, Params.h, Planner.h


class FilteredPointsTWD
{
protected:
  // Our NodeHandle
  ros::NodeHandle nh_;

public:
  double robottfx, robottfy, robottfth;
  double alfa, beta;
  int filtered = 0;
  int newSegment = 0;
  int amcl_pose_read = 0;
  double newPoint = 0.15; // udaljenost na kojoj se ubacuje nova tocka
	ros::Publisher filteredpath_pub;
	ros::Subscriber pathtwd_sub;
  ros::Subscriber amcl_pose_sub;

  pathtwdlistener::FilteredPath filpath;


  FilteredPointsTWD(ros::NodeHandle n) :
      nh_(n)
  {
 	pathtwd_sub = nh_.subscribe("twdPath",1,&FilteredPointsTWD::pathCallback, this);
	filteredpath_pub = nh_.advertise<pathtwdlistener::FilteredPath>("filtered_path",10);
  amcl_pose_sub = nh_.subscribe("/amcl_pose", 1, &FilteredPointsTWD::globalposeCallback, this); //minefield_pose za hratc
  }

  void publishnewfilteredmsg();
  void pathCallback( const twdplanner::PathConstPtr& pMsg);
  void globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);

};


int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "pathtwdlistener");
  ros::NodeHandle nh;
  FilteredPointsTWD filteredTWD(nh);

  ros::Rate rate(10.0);

  while (nh.ok()) {
    ros::spinOnce(); 

//	  filteredTWD.publishnewfilteredmsg();	
   
	  rate.sleep();
  }
  return 0;
}


void FilteredPointsTWD::publishnewfilteredmsg(){

			filteredpath_pub.publish(filpath);
}

int ispis = 0;
void FilteredPointsTWD::globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
  robottfx=msg->pose.pose.position.x;
 	robottfy=msg->pose.pose.position.y;
 	robottfth=tf::getYaw(msg->pose.pose.orientation);
  if (ispis==0){
	  ROS_INFO("\n First postion and orientation: x [%f], y [%f] and th [%f] deg\n", robottfx, robottfy, robottfth*180/M_PI);
    ispis++;
  }
}

void FilteredPointsTWD::pathCallback( const twdplanner::PathConstPtr& pMsg)
{
  if(filtered==0){

  geometry_msgs::Point p; 

  //FILTRIRANJE -> izbacivanje svih tocaka koje su na segmetu i spremanje pocetne i zavrsne tocke segmenta u novu poruku 
  filpath.points.clear();
  ROS_INFO("CISTIMO SVEEEEEE i printamo broj tocaka: %d", pMsg->points.size());

  beta = atan2(pMsg->points[1].y-pMsg->points[0].y, pMsg->points[1].x-pMsg->points[0].x);   //kut prvog segmeta
  ROS_INFO("Orijentacija prvog segmenta: %f", beta*180/M_PI);
      p.x=robottfx;
      p.y=robottfy;
      ROS_INFO("Filtiranje: prve tocka na putu: [%f %f]", p.x, p.y);
      filpath.points.push_back(p);
  // izbaivanje tocaka i poravnanje orijetacije robota sa segmentom
   for (int i=1; i<pMsg->points.size()-1; i++){

    // if (newSegment ==0){
    // //poravnanje orijentaije robota sa segmentom
    // // robottfth -> orijentacija robota; beta -> orijentacija segmenta
    // if ((robottfth-beta) == -M_PI/2){   // robot orijentiran za 90° u odnosu na segment
    //   //ubacivanje prve tocke u smjeru orijentacije robota
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y + newPoint;
    //   ROS_INFO("Ubacujem tocku na putu robot orijentiran za 90°: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    //   //ubacivanje druge tocke na x osi
    //   p.x=pMsg->points[i].x + newPoint;
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    // }
    // else if ((robottfth-beta) == M_PI/2){   // robot orijentiran za -90° u odnosu na segment
    //   //ubacivanje prve tocke u smjeru orijentacije robota
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y - newPoint;
    //   ROS_INFO("Ubacujem tocku na putu robot orijentiran za -90°: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    //   //ubacivanje druge tocke na x osi
    //   p.x=pMsg->points[i].x + newPoint;
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);    
    // }
    // else if((robottfth-beta) == M_PI || (robottfth-beta) == -M_PI ){   // robot orijentiran za 180° u odnosu na segment
    //   //ubacivanje prve tocke u smjeru orijentacije robota
    //   p.x=pMsg->points[i].x + newPoint;
    //   p.y=pMsg->points[i].y ;
    //   ROS_INFO("Ubacujem tocku na putu robot orijentiran za 180°: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    //   //ubacivanje druge tocke na x osi
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y + newPoint;
    //   ROS_INFO("Ubacujem drugu tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);
    //   //ubacivanje trece tocke koja je jednaka pocetnoj poziciji
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Ubacujem trecu tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);    
    // }
    // else if ((robottfth-beta) < 0 && (robottfth-beta) > -M_PI/2){   // robot orijentiran za kut 0,90° u odnosu na segment
    //   //ubacivanje prve tocke u smjeru orijentacije robota
    //   p.x=pMsg->points[i].x + newPoint;
    //   p.y=pMsg->points[i].y + newPoint*sin(robottfth-beta);
    //   ROS_INFO("Ubacujem tocku na putu robot orijentiran za kut 0,90° u odnosu na segment: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    //   //ubacivanje druge tocke na x osi
    //   p.x=pMsg->points[i].x + newPoint*cos(robottfth-beta);
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);
    // }
    // else if ((robottfth-beta) > 0 && (robottfth-beta) < M_PI/2){   // robot orijentiran za 0,-90° u odnosu na segment
    //   //ubacivanje prve tocke u smjeru orijentacije robota
    //   p.x=pMsg->points[i].x + newPoint;
    //   p.y=pMsg->points[i].y - newPoint*sin(robottfth-beta);
    //   ROS_INFO("Ubacujem tocku na putu robot orijentiran za 0,-90° u odnosu na segment: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    //   //ubacivanje druge tocke na x osi
    //   p.x=pMsg->points[i].x + newPoint*cos(robottfth-beta);
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);
    // }

    //     else if ((robottfth-beta) > M_PI/2 && (robottfth-beta) <M_PI){   // robot orijentiran za 90°,180°  u odnosu na segment
    //   //ubacivanje prve tocke u smjeru orijentacije robota
    //   p.x=pMsg->points[i].x - newPoint;
    //   p.y=pMsg->points[i].y + newPoint*sin(robottfth-beta);
    //   ROS_INFO("Ubacujem tocku na putu robot orijentiran za 90°,180°  u odnosu na segment: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    //   //ubacivanje druge tocke na y osi
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y + newPoint*cos(robottfth-beta);
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);
    //   //ubacivanje trece tocke na x osi
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);
    // }
    //   else if ((robottfth-beta) > -M_PI && (robottfth-beta) < -M_PI/2){   // robot orijentiran za -90°,-180° u odnosu na segment
    //   //ubacivanje prve tocke u smjeru orijentacije robota
    //   p.x=pMsg->points[i].x + newPoint*cos(robottfth-beta);
    //   p.y=pMsg->points[i].y - newPoint*sin(robottfth-beta);
    //   ROS_INFO("Ubacujem tocku na putu robot orijentiran za -90°,-180° u odnosu na segment: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);  
    //   //ubacivanje druge tocke na y osi
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y - newPoint*sin(robottfth-beta);
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);
    //   //ubacivanje trece tocke na x osi
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);
    // }
    // else{ //orijentacije su poravnete dodaj pocetnu tocku kao start
    //   p.x=pMsg->points[i].x;
    //   p.y=pMsg->points[i].y;
    //   ROS_INFO("Prva tocka na putu: [%f %f]", p.x, p.y);
    //   filpath.points.push_back(p);

    // }
    //alfa = atan2(pMsg->points[i+1].y-p.y, pMsg->points[i+1].x-p.x);   //atan2(y,x)
  //  newSegment = 1;
    //}
    //odredivanje kuta izmedu dva susjeda
   // if (newSegment == 0){
    alfa = atan2(pMsg->points[i+1].y-pMsg->points[i].y, pMsg->points[i+1].x-pMsg->points[i].x);   //atan2(y,x)
   // }

    if (alfa != beta){
      p.x=pMsg->points[i].x;
      p.y=pMsg->points[i].y;
      ROS_INFO("Filtiranje: tocka na putu %d: [%f %f]", i, p.x, p.y);
      filpath.points.push_back(p);
      beta = alfa;
      newSegment = 0;
    }else{
      p.x=pMsg->points[i].x;
      p.y=pMsg->points[i].y;
      ROS_INFO("Filtiranje: duplic na putu %d: [%f %f]", i, p.x, p.y);
    }
  }
  int end = pMsg->points.size();
      p.x=pMsg->points[end-1].x;
      p.y=pMsg->points[end-1].y;
      ROS_INFO("Filtiranje: zadnja tocka na putu: [%f %f]", p.x, p.y);
      filpath.points.push_back(p);


  ROS_INFO("Nakon filtiranja imamo broj tocaka: %d", filpath.points.size());
  // for (int i=0; i<filpath.points.size(); i++ ){
  //   p.x=filpath.points[i].x;
  //   p.y=filpath.points[i].y;
  //   ROS_INFO("Filtiranje: tocka na putu %d: [%f %f]", i, p.x, p.y);
  // }
  filteredpath_pub.publish(filpath);  //objavljivanje poruke na temu 
  filtered++;

  }
}
