#include <sys/time.h>
#include <iostream>
#include <string>
#include "ros/ros.h"

//#include <algorithm>

//#include <twdplanner/Point.h>
#include "geometry_msgs/Point.h"

#include <twdplanner/Path.h>
#include <pathtwdlistener/FilteredPath.h>
#include <pathtwdlistener/FixOrientation.h>


#include "moj.h"   //DStar.h, Params.h, Planner.h


struct T_point{
	double  x, y;
};

  T_point tr;
  std::vector<T_point> lastfilpath;

class FilteredPointsTWD
{
protected:
  // Our NodeHandle
  ros::NodeHandle nh_;
  tf::TransformListener tf_listener;

public:
  double robottfx, robottfy, robottfth;
  double alfa, beta, d2, theta;
  double velx,vely, velz;
  int filtered = 0;
  int fixed = 0;
  int newSegment = 0;
  int amcl_pose_read = 0;
  //double newPoint = 0.255; // udaljenost na kojoj se ubacuje nova tocka
  double newPoint = 0.712; // pioner RR =356.*2.
  double poseRot = 0;
  double golden = 1.618; //silver ratio 2.4142, bronze ratio 3.30277
  double du;

  double lasertfx2;//laser coordinates (global)
  double lasertfy2;
  double lasertfth2;

	ros::Publisher filteredpath_pub;
  ros::Publisher fixorietnation_pub;
	ros::Subscriber pathtwd_sub;
  ros::Subscriber amcl_pose_sub;
  ros::Subscriber fix_filteredpath_sub;
  ros::Subscriber laser_sub;
  ros::Subscriber vel_sub;

  pathtwdlistener::FilteredPath filpath;
  pathtwdlistener::FixOrientation fixpath;

  

  FilteredPointsTWD(ros::NodeHandle n) :
      nh_(n)
  {
  //pathtwd_sub = nh_.subscribe<patrolingGoals>("patroling_path",1,&FilteredPointsTWD::FilteredPathCallback, this); //patroling
 	pathtwd_sub = nh_.subscribe("twdPath",1,&FilteredPointsTWD::FilteredPathCallback, this); //TWD

	filteredpath_pub = nh_.advertise<pathtwdlistener::FilteredPath>("filtered_path",10);

  fix_filteredpath_sub = nh_.subscribe("filtered_path",1,&FilteredPointsTWD::FixPathCallback, this);
  fixorietnation_pub = nh_.advertise<pathtwdlistener::FixOrientation>("fix_path",10);
  amcl_pose_sub = nh_.subscribe("/amcl_pose", 1, &FilteredPointsTWD::globalposeCallback, this); 
  laser_sub = nh_.subscribe("/base_scan", 1, &FilteredPointsTWD::laserRearCallback, this); 

  vel_sub=nh_.subscribe<geometry_msgs::Twist>("cmd_vel", 1, &FilteredPointsTWD::velCallback, this);

  }
 // void publishnewfilteredmsg();
  void FilteredPathCallback( const twdplanner::PathConstPtr& pMsg);   //TWD
  //void FilteredPathCallback(const FilteredPointsTWD::patrolingGoalsConstPtr& pMsg); // patroling

  void FixPathCallback( const pathtwdlistener::FilteredPathConstPtr& pMsg);
  void globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
  void laserRearCallback(const sensor_msgs::LaserScan::ConstPtr& msg);
  void velCallback(const geometry_msgs::Twist::ConstPtr& msg);

};


int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "pathtwdlistener");
  ros::NodeHandle nh;
  FilteredPointsTWD filteredTWD(nh);

  lastfilpath.reserve(10000); 
  lastfilpath.clear();
  ros::Rate rate(10.0);

  while (nh.ok()) {
    ros::spinOnce(); 
    
//	  filteredTWD.publishnewfilteredmsg();	
   
	  rate.sleep();
  }
  return 0;
}


// void FilteredPointsTWD::publishnewfilteredmsg(){

// 			filteredpath_pub.publish(filpath);
// }
void FilteredPointsTWD::velCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
  velx=msg->linear.x;
 	vely=msg->linear.y;
 	velz=msg->linear.z;
	
  if(abs(velx) < 0.04 ){
    fixed = 0;
    newSegment = 0;

  }
}

int ispis = 0;
void FilteredPointsTWD::globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
  robottfx=msg->pose.pose.position.x;
 	robottfy=msg->pose.pose.position.y;
 	robottfth=tf::getYaw(msg->pose.pose.orientation);
  if (ispis==0){
	  ROS_INFO("\n First postion and orientation: x [%f], y [%f] and th [%f] deg\n", robottfx, robottfy, robottfth*180/M_PI);
    ispis++;
  }
}

void FilteredPointsTWD::laserRearCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
  
  	for(int i_LS=335;i_LS<385;i_LS++){
      if(msg->ranges[i_LS] < newPoint)
      {
        //printf("laser rages from %d is %f \n",i_LS, msg->ranges[i_LS]);
        poseRot = 1; //postavi zastavicu da je prepreka ispred robota
      }else{
        //printf("laser rages from %d is %f \n",i_LS, msg->ranges[i_LS]);
        poseRot = 0; //postavi zastavicu da je prepreka ispred robota
      }

    }
}


void FilteredPointsTWD::FilteredPathCallback(const twdplanner::PathConstPtr& pMsg) //TWD
//void FilteredPathCallback(const FilteredPointsTWD::patrolingGoalsConstPtr& pMsg)  //patroling
{
    geometry_msgs::Point p; 
    // for (int i=0; i<pMsg->points.size(); i++ ){
    //   p.x = pMsg->points[i].x;
    //   p.y = pMsg->points[i].y;
    //   ROS_INFO("FILTRIRANI PUT: tocka na putu %d: [%f %f]", i, p.x, p.y);
    // }

  //if(filtered==0){
  //FILTRIRANJE -> izbacivanje svih tocaka koje su na segmetu i spremanje pocetne i zavrsne tocke segmenta u novu poruku 
  filpath.points.clear();
  //ROS_INFO("CISTIMO SVEEEEEE i printamo broj tocaka prije filtriranja poruke: %d", pMsg->points.size());



  //ROS_INFO("Orijentacija prvog segmenta: %f", beta*180/M_PI);
    p.x=robottfx;
    p.y=robottfy;
   // ROS_INFO("Filtiranje: prve tocka na putu: [%f %f]", p.x, p.y);
    filpath.points.push_back(p);

  // beta = atan2(pMsg->points[2].y-pMsg->points[1].y, pMsg->points[2].x-pMsg->points[1].x);   //kut prvog segmeta
  beta = atan2(pMsg->points[1].y-pMsg->points[0].y, pMsg->points[1].x-pMsg->points[0].x);   //kut prvog segmeta

  // izbaivanje tocaka i poravnanje orijetacije robota sa segmentom
  for (int i=1; i<pMsg->points.size()-1; i++){
    alfa = atan2(pMsg->points[i+1].y-pMsg->points[i].y, pMsg->points[i+1].x-pMsg->points[i].x);   //atan2(y,x)
    //ROS_INFO("alfa i beta za %d korak: [%f %f]", i, alfa, beta);

    if (abs(alfa) > (abs(beta)-0.00001) && abs(alfa) < (abs(beta)+0.00001)){
      p.x=pMsg->points[i].x;
      p.y=pMsg->points[i].y;
      //ROS_INFO("Filtiranje: duplic na putu %d: [%f %f %f %f]", i, p.x, p.y, alfa, beta);
    }else{
      p.x=pMsg->points[i].x;
      p.y=pMsg->points[i].y;
      //ROS_INFO("Tocka na putu koja nije duplic %d: [%f %f %.15f %.15f]", i, p.x, p.y, alfa, beta);
      filpath.points.push_back(p);
      beta = alfa;
      newSegment = 0;
    }
  }
  int end = pMsg->points.size();
      p.x=pMsg->points[end-1].x;
      p.y=pMsg->points[end-1].y;
    //  ROS_INFO("Filtiranje: zadnja tocka na putu: [%f %f]", p.x, p.y);
      filpath.points.push_back(p);

 // ROS_INFO("Nakon filtiranja imamo broj tocaka: %d", filpath.points.size());
  ROS_INFO("filpath %d, lastfilpath %d", filpath.points.size(), lastfilpath.size());
  for (int i=1; i<filpath.points.size(); i++ ){
    ROS_INFO("%f %f %f %f", lastfilpath[i].x,lastfilpath[i].y, filpath.points[i].x, filpath.points[i].y);
    if (lastfilpath[i].x == filpath.points[i].x && lastfilpath[i].y == filpath.points[i].y){
      filtered = 1;
      ROS_INFO("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIISTI SU");
    }else{
       //lastfilpath[i].x = filpath.points[i].x;
       //lastfilpath[i].y = filpath.points[i].y;
      filtered = 0;
      break;
    }  
  }
  if(filtered == 0){
    lastfilpath.clear();
    for (int i=0; i<filpath.points.size(); i++ ){
      tr.x = filpath.points[i].x;
      tr.y = filpath.points[i].y;
      lastfilpath.push_back(tr);
    //   //ROS_INFO("FILTRIRANI PUT: tocka na putu %d: [%f %f]", i, p.x, p.y);
    }
    ROS_INFO("PPPPPPPPPPPPPPPPPPPPPPPPUUUUUUUUUUUUUUUUUUUUUBBB");
    filteredpath_pub.publish(filpath);  //objavljivanje poruke na temu 
  }
  //}
  //filtered = 0;
}

void FilteredPointsTWD::FixPathCallback(const pathtwdlistener::FilteredPathConstPtr& pMsg)
{
  // if( remove( "goal_position.txt" ) != 0 ){ //potrebno brisanje datoteke - ako se u prvom pozivu spremi vise tocaka nego u drugom, ostale tocke se gledaju kao ciljevi
  //   printf( "Error deleting file \n" );
  // }
  // else{
  //   printf( "File goal_position.txt successfully deleted! \n" );
  // }
  if (poseRot == 0){
  //citanje senzora. ako se napazi prepreka tocno ispred robota nek ipak rotira na mjestu, inace popravi kut
  geometry_msgs::Point p; 
  fixpath.points.clear();
  //ROS_INFO("CISTIMO SVEEEEEE i printamo broj tocaka za popravak orijentacije: %d", pMsg->points.size());
  if(filtered == 0){
  if(fixed==0){
  beta = atan2(pMsg->points[1].y-pMsg->points[0].y, pMsg->points[1].x-pMsg->points[0].x);   //kut prvog segmeta
  if (abs(beta)<0.02){
    beta = 0;
  }

  ROS_INFO("Orijentacija prvog segmenta: %f deg, %f rad", beta*180/M_PI, beta);

  p.x=pMsg->points[0].x;
  p.y=pMsg->points[0].y;
  //ROS_INFO("Ubacujem tocku na putu robot orijentiran za 90°: [%f %f]", p.x, p.y);
  fixpath.points.push_back(p); 

  // poravnanje orijetacije robota sa segmentom
   for (int i=0; i<pMsg->points.size()-1; i++){
      //golden = 1.618;
      d2 = newPoint*cos(robottfth-beta);

      if (abs(abs(robottfth)-abs(beta)) > M_PI/2 && (abs(abs(robottfth)-abs(beta)) < M_PI) || (abs(robottfth-beta) > M_PI/2 && abs(robottfth-beta) < 3*M_PI/2)|| ((abs(robottfth)+abs(beta)) > M_PI/2 && robottfth <0 && beta >0)){
        if (robottfth <0){
          d2 = newPoint * cos(robottfth + M_PI/2);
        }
        else if (robottfth >0){
          d2 = newPoint * cos(robottfth - M_PI/2);
        }
      //du = golden*golden * d2;
      }
      else if(abs(abs(robottfth)-abs(beta)) > 0 && abs(abs(robottfth)-abs(beta)) < M_PI/4){
        du = golden * d2;
      } 
      else if (abs(abs(robottfth)-abs(beta)) > 0 && abs(abs(robottfth)-abs(beta)) < M_PI/2) {
        du = golden*golden *d2;
      } 

     
    //ROS_INFO("Orijentacija : %f deg, %f rad", (robottfth-beta)*180/M_PI, (robottfth-beta));

    if (newSegment==0){

    //poravnanje orijentaije robota sa segmentom
    // robottfth -> orijentacija robota; beta -> orijentacija segmenta
    if (abs(robottfth-beta) > (M_PI/2 -0.02) && abs(robottfth-beta) < (M_PI/2+0.02) || (robottfth ==M_PI && (beta > -M_PI/2-0.02 && beta < -M_PI/2+0.02))){   // robot orijentiran za 90° u odnosu na segment
      //ubacivanje prve tocke u smjeru orijentacije robota
      p.x = pMsg->points[i].x + newPoint*cos(robottfth);
      p.y = pMsg->points[i].y + newPoint*sin(robottfth);
      ROS_INFO("Ubacujem tocku na putu robot orijentiran za 90°: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);  
      //ubacivanje druge tocke na x osi
      p.x = pMsg->points[i].x + newPoint*cos(beta);
      p.y = pMsg->points[i].y + newPoint*sin(beta);
      //ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);  
    }
    else if((robottfth-beta) == M_PI || (robottfth-beta) == -M_PI ){   // robot orijentiran za 180° u odnosu na segment
      //ubacivanje prve tocke u smjeru orijentacije robota
      p.x = pMsg->points[i].x + newPoint*cos(robottfth);
      p.y = pMsg->points[i].y ; //+ newPoint*sin(robottfth) -> nije potrebno dodati jer je 0 
      ROS_INFO("Ubacujem tocku na putu robot orijentiran za 180°: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);  
      //ubacivanje druge tocke 
      //prvi slucaj jednostvniji
      if(pMsg->points[1].y>0 || pMsg->points[1].x >0){
      p.x = pMsg->points[i].x + newPoint*cos(3*M_PI/4);
      p.y = pMsg->points[i].y + newPoint*sin(3*M_PI/4);
      }
      else{
      p.x = pMsg->points[i].x + newPoint*cos(M_PI/4);
      p.y = pMsg->points[i].y + newPoint*sin(M_PI/4);
      
      }
      //drugi slucaj - teze za odozi
      //p.x = pMsg->points[i].x;
      //p.y = pMsg->points[i].y + newPoint*sin(robottfth);
      ROS_INFO("Ubacujem drugu tocku na putu: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);
      //ubacivanje trece tocke na x osi
      p.x = pMsg->points[i].x + du*cos(beta);  
      p.y = pMsg->points[i].y + du*sin(beta);

      ROS_INFO("Ubacujem trecu tocku na putu: [%f %f], du: %f", p.x, p.y, du);
      fixpath.points.push_back(p);    
    }

    else if (abs(robottfth-beta) > M_PI/2 && abs(robottfth-beta) < 3*M_PI/2){   // robot orijentiran za 90°,180°  u odnosu na segment
      //ubacivanje prve tocke u smjeru orijentacije robota
      p.x = pMsg->points[i].x + newPoint*cos(robottfth);
      p.y = pMsg->points[i].y + newPoint*sin(robottfth);
      ROS_INFO("Ubacujem tocku na putu robot orijentiran za 90°,180°  u odnosu na segment: [%f %f]", p.x, p.y);
      //ROS_INFO("du: [%f]", du);

      fixpath.points.push_back(p);  
      //ubacivanje druge tocke na y osi
      theta = atan2(pMsg->points[1].y - p.y, pMsg->points[1].x - p.x);  //odredivanje lijeve ili desne rotacije
      ROS_INFO("Rotacija lijevo ili desno: [theta = %f, robotttfth = %f, beta = %f]", theta*180/M_PI, robottfth*180/M_PI, beta*180/M_PI);
      if (theta > 0){
        if ((abs(robottfth) > abs(beta) && robottfth > 0 && beta > 0) || (abs(robottfth) < abs(beta) && robottfth < 0 && beta < 0) || (robottfth <= -M_PI/2 && beta > M_PI/2)){
            p.x = pMsg->points[i].x + newPoint*cos(robottfth - M_PI/4);
            p.y = pMsg->points[i].y + newPoint*sin(robottfth - M_PI/4);
            //ROS_INFO("[2. tocka na -pi/4 :%f %f]", p.x, p.y);
        }
        else {
          p.x = pMsg->points[i].x + newPoint*cos(robottfth + M_PI/4);
          p.y = pMsg->points[i].y + newPoint*sin(robottfth + M_PI/4);
          //ROS_INFO("[2. tocka na +pi/4 :%f %f %f %f]", p.x, p.y, robottfth, beta) ;

        }
      } 
      else{
        if (abs(robottfth) > abs(beta) && !(robottfth < 0 && beta < 0) || abs(robottfth) < abs(beta) && (robottfth < 0 && beta < 0)){
            p.x = pMsg->points[i].x + newPoint*cos(robottfth - M_PI/4);
            p.y = pMsg->points[i].y + newPoint*sin(robottfth - M_PI/4);
            //ROS_INFO("[%f %f]", p.x, p.y);
        }
        else {
          p.x = pMsg->points[i].x + newPoint*cos(robottfth + M_PI/4);
          p.y = pMsg->points[i].y + newPoint*sin(robottfth + M_PI/4);
        }
      }

      //ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);
      //ubacivanje trece tocke na x osi
      double zeta = atan2(pMsg->points[1].y - p.y, pMsg->points[1].x - p.x); 
      if(zeta > 0 && zeta < M_PI/4){
        du = golden * d2;
      } 
      else {
        du = golden*golden *d2;
      } 
      p.x = pMsg->points[i].x + du*cos(beta);  
      p.y = pMsg->points[i].y + du*sin(beta);
      //ROS_INFO("prvi uvjet: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);
    }

    else if (abs(abs(robottfth)-abs(beta)) >0 && abs(abs(robottfth)-abs(beta)) < M_PI/2){   // robot orijentiran za kut 0,90° u odnosu na segment
      ROS_INFO("Orijentacija : %f deg, %f rad", (robottfth-beta)*180/M_PI, (robottfth-beta));
      //ubacivanje prve tocke u smjeru orijentacije robota
      p.x = pMsg->points[i].x + newPoint*cos(robottfth);
      p.y = pMsg->points[i].y + newPoint*sin(robottfth);
      ROS_INFO("Ubacujem tocku na putu robot orijentiran za kut 0,90° u odnosu na segment: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);  
      p.x = pMsg->points[i].x + du*cos(beta);  
      p.y = pMsg->points[i].y + du*sin(beta);

      //ROS_INFO("Ubacujem tocku na putu: [%f %f]", p.x, p.y);
      fixpath.points.push_back(p);
    }
   //spremanje tocaka u datoteku
  FILE	*goldenlogfile;
  if ( (goldenlogfile = fopen("golden_points.txt","w")) == NULL ){
	  printf("Error! Input file couldn't be opened.");
	}else{
    for (int i=0; i < fixpath.points.size(); i++ ){
      p.x = fixpath.points[i].x;
      p.y = fixpath.points[i].y;
		  fprintf(goldenlogfile,"%f %f \n",p.x,p.y);
    }
	}
	if (goldenlogfile!=NULL){
			  fclose(goldenlogfile);
	}

    newSegment = 1;
    }// newSegement -> sluzi da bi se samo pocetna orijentacija poravnala s prvim segmentom

    else{ //orijentacije su poravnete dodaj pocetnu tocku kao start
      p.x = pMsg->points[i].x;
      p.y = pMsg->points[i].y;
      //ROS_INFO("Dodajem tocku na put. %d segment: [%f %f]", i,p.x, p.y);
      fixpath.points.push_back(p);
    }
  }
   
  int end = pMsg->points.size();
  p.x=pMsg->points[end-1].x;
  p.y=pMsg->points[end-1].y;
  ROS_INFO("Zadnja tocka na putu: [%f %f]", p.x, p.y);
  fixpath.points.push_back(p);
    fixed++;
  fixorietnation_pub.publish(fixpath);  //objavljivanje poruke na temu 
  }
  else{
       for (int i=0; i<pMsg->points.size(); i++){
        p.x=pMsg->points[i].x;
        p.y=pMsg->points[i].y;
        fixpath.points.push_back(p);
  }
  fixorietnation_pub.publish(fixpath);  //objavljivanje poruke na temu 

//  filtered = 0;

  }
    

  //spremanje tocaka u datoteku
  FILE	*goallogfile;
  if ( (goallogfile = fopen("goal_position.txt","a")) == NULL ){
	  printf("Error! Input file couldn't be opened.");
	}else{
    for (int i=0; i < fixpath.points.size(); i++ ){
      p.x = fixpath.points[i].x;
      p.y = fixpath.points[i].y;
		  fprintf(goallogfile,"%f %f \n",p.x,p.y);
    }
	}
	if (goallogfile!=NULL){
			  fclose(goallogfile);
	}
  
  }
}
}