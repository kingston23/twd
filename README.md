# TWD Planner

This README explains starting and using the TWD Planner.

### Set params

To use package `pathtwdlistener` for patrolling with twd replanning set in CMakeLists.txt `add_executable(pathls src/pointfilter.cpp)` as active:

```
add_executable(pathls src/pointfilter.cpp)
#add_executable(pathls src/pointfilter_patroling.cpp)
#add_executable(pathls src/pointfilter_bez_pocetne_orijentacije.cpp)
``` 

In `twdsrc/twdpanner/include` set in `Params.h` :
//inverted laser on husky
#define LASER_INVERTED 0
//width of the robot (acc. y axis)
#define ROBOT_MASK (ceil(RR/CELL_DIM))
//length of the robot (acc. x axis)
#define ROBOT_MASKY (ceil(RRY/CELL_DIM))
//reading from the map topic (png) or parameters
#define LOADMAP 1   // za twd koji nije za patrolling staviti 1, za pratrolling 0

In `twdsrc/twdpanner/include` set in `moj.h` fixed_frame to `map`:

```
VisualizationPublisher(ros::NodeHandle n) :
      nh_(n),  fixed_frame_("map") //map kad koristis stage, odom u gazebu simulacija, minefield gazebo kasnija simulacija
```

In `maintwd.cpp` set robot model to circular/pioneer.
In `smoothedpathlistener/main.cpp` set `target_frame("map")`

In `nodelet_talker/CMakeLists.txt` use:

```
## Declare a C++ library
add_library(nodelet_talker
#src/talker.cpp
#src/90deg_test_talker.cpp
src/twd_talker.cpp
#src/ccpp_talker.cpp
#src/patroling_talker.cpp
#src/patroling_talker_test.cpp
)
```

### Getting started

* Put the whole twdsrc folder into your catkin workspace's src folder. The folder `twdplanner` is the TWD planner, while the folder `pathtwdlistener` is the simple subscriber to the output message of the TWD planner. 
* Compile the source by typing `catkin_make` in the root folder of your catkin workspace
* Open three terminals and start:

1. `roslaunch twdplanner ushapeamcltest.launch`

2. `roslaunch twdplanner starttwd.launch`

3. `roslaunch pathtwdlistener startpathlistener.launch`


To use smoothing with TWD* run:

4. `roslaunch nodelet_talker test1.launch`

5. `roslaunch nodelet_talker talker.launch`

6. `rosrun acado_traj drivetraj`


Click on the rviz window on `2D Nav Goal` button and then click somewhere in the free space of the loaded environment. The robot should start to move to the selected goal point. The TWD path should be plotted on the rviz window.

### Launch scripts explained

There are three prepared Stage simulations in `twdplanner/map` folder and prepared launch files in `twdplanner/launch` folder.

1. roslaunch twdplanner ushapeamcltest.launch

2. roslaunch twdplanner clearmaplargeamcltest.launch

3. roslaunch twdplanner aulatest.launch

Each simulation has a simulated differential drive circular-shaped robot and a green box simulating the moving obstacle. The first map has an U-shape obstacle in small-size environment. The second map has only border obstacles and a box for testing replanning of the TWD algorithm. The third map has a large floorplan of FER buildings.

After loading a robot and a map, the TWD algorithm can be started by typing:

```
roslaunch twdplanner starttwd.launch
```

This starts the twd node with the following parameters:

```
    <node name="twd" pkg="twdplanner" type="twd" output="screen">
        <param name="base_frame" value="base_link" />
        <param name="odom_topic" value="/odom" />
        <param name="scan_topic" value="/base_scan" />
        <param name="world_frame" value="map" />
        <param name="cmd_vel_topic" value="cmd_vel" />
        <param name="vx_min" type="double" value="0" />
        <param name="vx_max" type="double" value="750." />
        <param name="dvx_max" type="double" value="500." />
        <param name="w_min" type="double" value="-100." />
        <param name="w_max" type="double" value="100." />
        <param name="dw_max" type="double" value="100." />
        <param name="robot_radius" type="double" value="400." />
        <param name="drive_flag" type="bool" value="true" />
    </node>
```

Most of the parameters are self explained such as standard ROS tf topics, maximal and minimal velocities and accelerations, while `robot_radius` is the radius of the outer circumscribed robot footprint in millimeters and `drive_flag` is an on/off flag if the robot will move to the goal using the Dynamic Window approach, or will not move but instead one can move it by drag and drop in Stage simulator.

### Writing a subscriber to the TWD path message

A simple subscriber is already prepared in the package `pathtwdlistener`. It subscribes to the output message of the TWD planner and can be started by typing:

```
roslaunch pathtwdlistener startpathlistener.launch
``` 

In this package there is an example code of subscribing to the message of the `twdplanner` package and writing the callback function to get the whole path message and to visualize it in rviz. This can serve as an example for writing your own subscriber to the path message. Important parts from `pathtwdlistener/main.cpp` is here:

First, include the message header:

```
#include <twdplanner/Path.h>
```

Second, subscribe to the topic:

```
pathtwd_sub = nh_.subscribe("twdPath",1,&VisualizationPublisherTWD::pathCallback, this);
```

Then write a callback:
```
void VisualizationPublisherTWD::pathCallback(const twdplanner::PathConstPtr& pMsg)
{
  pathvis.points.clear();
  geometry_msgs::Point p; 
	for (int i=0; i<pMsg->points.size(); i++){
		p.x=pMsg->points[i].x;
		p.y=pMsg->points[i].y;
		pathvis.points.push_back(p);
	}
}
```

* you also need to add in `CMakeLists.txt` of your package the line _twdplanner_ in find_package function otherwise the compiler will complain it can not find the header file:
```
find_package(catkin REQUIRED COMPONENTS
  std_msgs
  nav_msgs
  geometry_msgs
  message_generation
  roscpp
  tf
  twdplanner
)
```
* add depend _twdplanner_ in your package in `package.xml`:
```
  <build_depend>twdplanner</build_depend>
  <run_depend>twdplanner</run_depend>
```

The TWD planner calculates the path only when the new goal is set. This can be done in rviz through `2D Nav Goal` button or by writing the simple action client as is explained in <http://wiki.ros.org/actionlib>.


### Create your own map

Create a bitmap in png or pgm format and put it to twdplanner/map/bitmaps. Create a yaml file and put it in twdplanner/map with the following text:

```
#demo.yaml
image: bitmaps/demo.png
resolution: 0.014005602
origin: [-9.916, -6.12, 0.0]
negate: 0
occupied_thresh: 0.65
free_thresh: 0.196

```
where

* the image name is `demo.png` in the correct relative path from where `demo.yaml` is located
* **resolution** defines the size of the pixel of the png file in meters. To calculate the resolution, you need to know the width of the image in meters. Then, simply divide the width in meters with the width in the number of pixels. 
* **negate** can inverse the free-occupied values if set to 1
* **occupied_thresh** and **free_thresh** define the thresholds that tell which brightness level should be considered as occupied and free, respectively. 
* **origin** is the (x,y,z) vector in meters describing the lower-left corner (that is the reason why the example coordinates have negative sign since the origin is somewhere inside the image). With (0,0,0) the origin will be in the lower-left corner.

Then, prepare the Stage world file, for example `demo.world` and put it to `twdplanner/map` folder:
```
# the size of a pixel in Stage's underlying raytrace model in meters
resolution 0.01

interval_sim 100  # milliseconds per update step

# defines Pioneer-like robots
include "pioneer.inc"

# defines 'map' object used for floorplans
include "map.inc"
include "sick2.inc"

window( size [ 569 392 ] center [3.287 4.369] scale 19.013 )


floorplan
(
  name "floorplan"
  bitmap "bitmaps/demo.png"
  size [ 26.89 15.126 1.000 ]
  pose [ 3.53 1.443 0.000 0.000 ] 
)


# create a robot
pioneer2dx
(
  pose [ 1.0 0 0.000 0.000 ] 
  name "robot_0"
  color "blue"  
  sicklaser(
     pose [0.400 0.000 0.000 0.000]
     # ctrl "lasernoise"  # uncomment this line to run a laser noise generator
  )
  localization_origin [ 0 0 0 0 ]
)

define box model
(
  size [0.300 0.500 0.500]
  laser_return 1 
  obstacle_return 1
  gui_nose 0
)

box( pose [ -2.538 1.380 0.000 180.000 ] color "green")
```
The most important parameters are **size** and **pose** in the **floorplan** section which need to be calculated in meters from the pixel sizes of `demo.png` and **resolution** in `demo.yaml`.

*	**size** is [x y z] size of the map in meters, obtained by multiplication of **resolution** in `demo.yaml` with widht and height in pixels.

*	**pose** is [x y z theta] of the origin of the map calculated from the middle of the floorplan. To align the origin of the `demo.yaml` with the origin of the Stage use the following equation:

```
x= (x of size of the floorplan)/2 + (x of origin in demo.yaml)
y= (y of size of the floorplan)/2 + (y of origin in demo.yaml)
z=0
theta=0
```

*	**pose** of the **robot** can be chosen arbitrarly since the fake amcl localization is used.
*	Choose the **pose** of the **box**. It is here treated as moving obstacle.
*	The rest of the parameters can be the same as in this example. For more explanation about the **world** files visit <http://wiki.ros.org/stage>. 

Create the `demotest.launch` file and put it into `twdplanner/launch` folder:

```
<launch>
  <master auto="start"/>
  <param name="/use_sim_time" value="true"/>

<node name="map_server" pkg="map_server" type="map_server" args="$(find twdplanner)/map/demo.yaml" respawn="false" >
<param name="frame_id" value="/map" />
</node>

<node pkg="stage_ros" type="stageros" name="stageros" args="$(find twdplanner)/map/demo.world" respawn="false" >
    <param name="base_watchdog_timeout" value="0.2"/>
</node>

<include file="$(find twdplanner)/launch/fake_corridor.launch" />

<node name="rviz" pkg="rviz" type="rviz" args="-d $(find twdplanner)/stagesim.rviz" /> 

</launch>
```

As localization a fake amcl is used whose parameters are in `twdplanner/launch/fake_corridor.launch` file. To test on a real robot an amcl localization should be used with initial robot location set as explained here: <http://wiki.ros.org/amcl>.

Start the newly created simulation by typing:

```
roslaunch twdplanner demotest.launch
```

### Examining the log files in Matlab

TWD planner saves all the data to files when the robot reaches the goal. These files are saved in `~/.ros/logger` folder. Copy and paste m-scripts from `twdplanner/logger` folder to that one. There are 3 scripts:

1. crtanjePutanjereplan.m - draws initial path and all replanning path with the optimal area of equal costs D* path

2. putanjeintegeritwd.m - draws D and TWD path paused during execution

3. izracuntwd.m - draws execution times of D, reverse D, and TWD path



