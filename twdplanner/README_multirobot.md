# Dodavanje prepreke

Robot_1 koristi se kao gibajuca prepreka.

### Potrebne izmjene prije pokretanja skripti

* U `ushapeamcltest.world` dodati novog robota 

# stvaranje novog robota - definiranje pocetne pozicije naredbnom  pose [x y z thet] 

```

pioneer2dx
(
name "zeleni"
  color "green"
 pose [2.4 3.1 0.000 90.0000] 
sicklaser()
  localization_origin [0 0 0 0]
velocity_bounds [-2 2 0 0 0 0 -200 200 ]
acceleration_bounds [-0.2 0.2 0 0 0 0 -20 20]
  size [0.44 0.38 0.32] #set z from 0.22 to 0.32 for laser detection of the 2. robot 
)

```

Napraviti izmjene u `starttwd.launch`:

```
    <node name="twd" pkg="twdplanner" type="twd" output="screen">
        <param name="base_frame" value="/robot_0/base_link" />
        <param name="scan_topic" value="/robot_0/base_scan" />
        <param name="cmd_vel_topic" value="/robot_0/cmd_vel" />
	...
        <param name="drive_flag" type="bool" value="true" />
    </node>

```

`drive_flag` is an on/off flag if the robot will move to the goal using the Dynamic Window approach, or will not move but instead one can move it by drag and drop in Stage simulator

U `maintwd.cpp` postaviti waitForTransform i lookupTransform na /robot_0/base_link:

```
tf_listener.waitForTransform("/robot_0/base_link",msg->header.frame_id, msg->header.stamp, ros::Duration(5));
tf_listener.lookupTransform("/robot_0/base_link",msg->header.frame_id, ros::Time(0), transform_laser);

```

U `moj.h` postaviti cmd_vel_topic_:

```
      cmd_vel_topic_ = "/robot_0/cmd_vel";

```

### Koraci za pokretanje twd planera:

1. roslaunch twdplanner ushapeamcltest_multi.launch

2. roslaunch twdplanner starttwd_multi.launch







