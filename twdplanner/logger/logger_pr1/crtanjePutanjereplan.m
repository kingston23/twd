% clear
WH_dstar_cost_map=load('wh_dstar_cost_map.dat');
skupine=load('wh_dstar_skupine_map.dat');
    WH_gridmap_x=load('wh_gridmap_x.dat');
	WH_gridmap_y=load('wh_gridmap_y.dat');

brojskupina=max(max(skupine));
origin=load('origin.dat');
Map_Home_x=origin(1);
Map_Home_y=origin(2);
xorigin=Map_Home_x;%player-stage
yorigin=Map_Home_y;
metric=0.001;
cell=load('cell_size.dat');
WH_planner_globalna_putanja_x=(load('global_planner_path_x.dat')-Map_Home_x)/cell;
WH_planner_globalna_putanja_y=(load('global_planner_path_y.dat')-Map_Home_y)/cell;

cijena_prepreke=max(max(WH_dstar_cost_map));
Dstar_path_x=(load('wh_dstar_path_x.dat')-Map_Home_x)/cell;
Dstar_path_y=(load('wh_dstar_path_y.dat')-Map_Home_y)/cell;
crtanjeoptpodrucja=1; %1-da 0-ne
crtanjeskupina=0; %1 da 0 ne, jedno i drugo ne smije bit ukopcano
crtajsamozadnju=0; %0 i predzadnju DD i D
crtajzadnjihnekoliko=0; %0 kad necu da se crtaju predzadnje nego sve twd
crtajsveputanje=1; %sve zive logirane, crtajditwd mora bit 1
crtajditwd=0;%0 ne obje nego samo twd
crtajsamotwd=1;%samo twd, mora biti crtajsveputanje=1 i crtajditwd=0

trenutna_planner_putanja_x=(load('global_planner_path_current_x.dat')-Map_Home_x)/cell;%sljedeci Pinitial
trenutna_planner_putanja_y=(load('global_planner_path_current_y.dat')-Map_Home_y)/cell;
pathpointeri=load('wh_svipathpointeri.dat');
goal=load('wh_goal_position.dat');
start=load('wh_start_position.dat');
% goal=[496 250];

poc=1;
prviput=1;
prosla=1;
proslaD=1;
proslaD2=1;
for i=1:max(size(pathpointeri)),
     if (pathpointeri(i)~=0)
 kraj=pathpointeri(i)+poc-1;
 if (i==max(size(pathpointeri))-1)
     prviput=0;
     trenutna_x=trenutna_planner_putanja_x(poc:kraj);
     trenutna_y=trenutna_planner_putanja_y(poc:kraj);
 end
 if (i==max(size(pathpointeri))-3)
     prosla=0;
     prosla_x=trenutna_planner_putanja_x(poc:kraj);
     prosla_y=trenutna_planner_putanja_y(poc:kraj);
 end
 if (i==max(size(pathpointeri)))
     proslaD=0;
     proslaD_x=trenutna_planner_putanja_x(poc:kraj);
     proslaD_y=trenutna_planner_putanja_y(poc:kraj);
 end
 if (i==max(size(pathpointeri))-2)
     proslaD2=0;
     proslaD2_x=trenutna_planner_putanja_x(poc:kraj);
     proslaD2_y=trenutna_planner_putanja_y(poc:kraj);
 end
 poc=kraj+1;
 end
end


[sizex,sizey]=size(WH_dstar_cost_map);
veca=zeros(sizex,sizey);
Xos=[0:1:sizex-1];
Yos=[0:1:sizey-1];
xv=[0:1:(sizex+9)];
yv=[0:1:(sizey+9)];

figure
veca(1:sizex,1:sizey)=WH_dstar_cost_map;
WH_dstar_f_cost_map=load('wh_dstar_f_cost_map.dat');
% WH_dstar_f_cost_map=load('wh_wit_f_cost.dat');
minf=100000;
   for x=1:sizex,
       for y=1:sizey,
	   if WH_dstar_f_cost_map(x,y)<minf && WH_dstar_f_cost_map(x,y)>0
               minf=WH_dstar_f_cost_map(x,y);
           end
       end
   end
   minf
%    goal(1)=WH_planner_globalna_putanja_x(end)-0.5;
% goal(2)=WH_planner_globalna_putanja_y(end)-0.5;
   minf=WH_dstar_f_cost_map(goal(1)+1,goal(2)+1)

%    minf=WH_dstar_f_cost_map(start(1)+1,start(2)+1)
if (crtanjeoptpodrucja)
   for x=1:sizex,
       for y=1:sizey,
       if WH_dstar_f_cost_map(x,y)==minf
           veca(x,y)=0;%minf;
       end
       end
   end
end
if (crtanjeskupina)
   for x=1:sizex,
       for y=1:sizey,
       if skupine(x,y)>0
           veca(x,y)=cijena_prepreke+skupine(x,y);%minf;
       end
       end
   end
end

%  xmin=4; xmax=50; ymin=15; ymax=29;%ppt icat robot representation
% xmin=55; xmax=120; ymin=80; ymax=105;
xmin=1; xmax=sizex; ymin=1; ymax=sizey;
%ovdje crtamo malu matricu karticu
if (1)
 osi=[xmin xmax ymin ymax];
kartica=veca;
 Xos=xv(osi(1):osi(2))+1;
Yos=yv(osi(3):osi(4))+1;
% Xos_real=Xos*cell+xorigin;
% Yos_real=Yos*cell+yorigin;
kartica=kartica(Xos,Yos);
Xos=Xos*cell+xorigin;
Yos=Yos*cell+yorigin;
k=linspace(1,0.4,cijena_prepreke-1);
if cijena_prepreke==2
    k=1;
end
if (crtanjeoptpodrucja)
   colormap([0.8 0.8 1;[k' k' k'];0 0 0]);
else
   colormap([1 1 1;[k' k' k'];0 0 0]);
end
if (crtanjeskupina)
      colormap([[k' k' k'];0 0 0;hsv(brojskupina)]);
end 

% colormap([k]);
% pcolor(x,y,WH_dstar_f_cost_map','EdgeColor','none');
surf(Xos*metric,Yos*metric,kartica'*0,kartica','EdgeColor','none')
% surf(Xos*metric,Yos*metric,kartica'*0,kartica','EdgeColor',[0.7 0.7 0.7])%,'EdgeColor','none') %robot representation
if (crtanjeskupina)
caxis([1 cijena_prepreke+brojskupina])
else
caxis([0 cijena_prepreke])
end

grid off
% view([90 -90]);
end
grid off
view([0 90]);
% surf(xv*metric,yv*metric,veca'*0,veca','EdgeColor','none')
% caxis([minff-2 maxff])
hold on

plot((WH_gridmap_x+1.5*cell)*metric,(WH_gridmap_y+1.5*cell)*metric,'r.','MarkerSize',0.1);

if 0 %robot representation
[yos,xos]=find(gridmap'==2);
xi=find(xos<osi(2));%gornja granica
xid=find(xos(xi)>=osi(1));%donja granica
yi=find(yos(xi(xid))<osi(4));%gornja po y-u
yid=find(yos(xi(xid(yi)))>=osi(3));
yos=yos(xi(xid(yi(yid))));
xos=xos(xi(xid(yi(yid))));
for n=1:length(yos)
%     xv=[xos(n) (xos(n)+1)]*cell+xorigin;
%     yv=[yos(n) (yos(n)+1)]*cell+yorigin;
%     a=ones(2);
%     surf(xv*metric,yv*metric,a*0,a,'EdgeColor','none','FaceColor',[1 0 0]);
    xr=(xos(n)+0.5)*cell+xorigin;
    yr=(yos(n)+0.5)*cell+yorigin;
%     plot(xos(n)+0.5,yos(n)+0.5,'rx');
    plot(xr*metric,yr*metric,'wx','Linewidth',1.5);%ifac zakomentirao
end
end
    rr=2.60;%kruzici za crtanje pocetne i trenutne pozicije
	fi=linspace(0,2*pi,30);
	x_temp=proslaD_x(1)+1;
    y_temp=proslaD_y(1)+1;
    th_temp=-20*pi/180;%WH_globalna_putanja_th(end);
% x_temp=WH_globalna_putanja_x(end); y_temp=WH_globalna_putanja_y(end)+2;
    for i=1:30
		x_circle(i)=x_temp+rr*cos(fi(i));
		y_circle(i)=y_temp+rr*sin(fi(i));
    end
	plot((x_circle*cell+xorigin)*metric,(y_circle*cell+yorigin)*metric,'r--','Linewidth',1.5);
% 	plot(([x_temp x_temp+rr*cos(th_temp)]*cell+xorigin)*metric, ([y_temp y_temp+rr*sin(th_temp)]*cell+yorigin)*metric,'k-','Linewidth',1.5);
%     x_square=[x_temp-3 x_temp+3 x_temp+3 x_temp-3 x_temp-3];
%     y_square=[y_temp-3 y_temp-3 y_temp+3 y_temp+3 y_temp-3];
% x_temp=1.5; y_temp=147.5;
velki=0.5;
    x_square=[x_temp-velki x_temp+velki x_temp+velki x_temp-velki x_temp-velki];%ifac
    y_square=[y_temp-velki y_temp-velki y_temp+velki y_temp+velki y_temp-velki];
	plot((x_square*cell+xorigin)*metric,(y_square*cell+yorigin)*metric,'r','Linewidth',1.5);
velki=3.5;
    x_square=[x_temp-velki x_temp+velki x_temp+velki x_temp-velki x_temp-velki];%ifac
    y_square=[y_temp-velki y_temp-velki y_temp+velki y_temp+velki y_temp-velki];
    
	plot((x_square*cell+xorigin)*metric,(y_square*cell+yorigin)*metric,'r--','Linewidth',1.5);
% save Dstar_put1 Dstar_realna_x Dstar_realna_y
if 0 %robot representation
text(-22.5,-22.6,'5','fontsize',12)
text(-22.6,-22.5,'4','fontsize',12)
text(-22.7,-22.4,'3','fontsize',12)
text(-22.8,-22.3,'2','fontsize',12)
text(-22.9,-22.2,'1','fontsize',12)
text(-23.1,-22.2,'o =','fontsize',12)
% text(-22.3,-22.9,'\infty','fontsize',12)
text(-23.7,-22.43,'robot','fontsize',12)
text(-23.7,-22.56,'representation','fontsize',12)
% text(-23.6,-22.23,'robot squared mask','fontsize',12)
text(-23,-23.4,'D* path','fontsize',12)
text(-23,-23.,'E* path','fontsize',12)
end


for i=2:length(WH_planner_globalna_putanja_x)
    if WH_planner_globalna_putanja_x(i)==WH_planner_globalna_putanja_x(i-1) && WH_planner_globalna_putanja_y(i)==WH_planner_globalna_putanja_y(i-1)
   plot(((WH_planner_globalna_putanja_x(i)+1)*cell+xorigin)*metric,((WH_planner_globalna_putanja_y(i)+1)*cell+yorigin)*metric,'c-*','LineWidth',1.5)
    end
end 

if (crtajzadnjihnekoliko)
if (crtajsamozadnju==0)
plot(((WH_planner_globalna_putanja_x+1)*cell+xorigin)*metric,((WH_planner_globalna_putanja_y+1)*cell+yorigin)*metric,'b','LineWidth',1.5)
% plot(((Dstar_path_x+1)*cell+xorigin)*metric,((Dstar_path_y+1)*cell+yorigin)*metric,'g','LineWidth',1.5)
if (proslaD2==0)
plot(((proslaD2_x+1)*cell+xorigin)*metric,((proslaD2_y+1)*cell+yorigin)*metric,'y','LineWidth',1.5)
end
end
if (prosla==0)
plot(((prosla_x+1)*cell+xorigin)*metric,((prosla_y+1)*cell+yorigin)*metric,'g*-','LineWidth',1.5)
% plot(((prosla_x(141:end)+1)*cell+xorigin)*metric,((prosla_y(141:end)+1)*cell+yorigin)*metric,'m*','LineWidth',1.5)
end
if (proslaD==0)
plot(((proslaD_x+1)*cell+xorigin)*metric,((proslaD_y+1)*cell+yorigin)*metric,'g-','LineWidth',1.5)
end
if (prviput==0)
if 1
    plot(((trenutna_x+1)*cell+xorigin)*metric,((trenutna_y+1)*cell+yorigin)*metric,'k-','LineWidth',1.5)
else
    for i=22:25
    plot(((trenutna_x(i)+1)*cell+xorigin)*metric,((trenutna_y(i)+1)*cell+yorigin)*metric,'c-*','LineWidth',1.5)
pause
    end
end
end
end
if crtajsveputanje
    poc=1;
for i=1:max(size(pathpointeri)),
     if (pathpointeri(i)~=0)
 kraj=pathpointeri(i)+poc-1;
 poc
 kraj
 i
     trenutna_x=trenutna_planner_putanja_x(poc:kraj);
     trenutna_y=trenutna_planner_putanja_y(poc:kraj);
     if crtajditwd || (crtajsamotwd && mod(i,2)==0)
 plot(((trenutna_x+1)*cell+xorigin)*metric,((trenutna_y+1)*cell+yorigin)*metric)
     end
 poc=kraj+1;
%  pause
 end
end
end



if 0 %robot representation
for ii=1:length(I)
    if mod(ii,10)==1
    plot((I(ii,1)+cell)*metric,(I(ii,2)+cell)*metric,'b-*','LineWidth',1.5)
    end
end
end

%provjera zbrajanja
if (1)
%     WH_dstar_k_cost_map=load('wh_dstar_k_cost_map.dat');
%     WH_dstar_h_cost_map=load('wh_dstar_h_cost_map.dat');
% 
% WH_dstar_k_cost_map(trenutna_x(23)+0.5,trenutna_y(23)+0.5)
suma=0;
    p=[1 1];q=p; %inicijalizacija
    korak=0.1; %desetinka polja (tako je za E* namjesteno kod odredjivanja puta)
for i=2:length(WH_planner_globalna_putanja_x)
        dodatak=sqrt((WH_planner_globalna_putanja_x(i)-WH_planner_globalna_putanja_x(i-1))^2+(WH_planner_globalna_putanja_y(i)-WH_planner_globalna_putanja_y(i-1))^2)*0.1;
    if cijena_prepreke>2
    p(1)=WH_planner_globalna_putanja_x(i-1)-0.5;%integeri u matrici
    p(2)=WH_planner_globalna_putanja_y(i-1)-0.5;
    q(1)=WH_planner_globalna_putanja_x(i)-0.5;
    q(2)=WH_planner_globalna_putanja_y(i)-0.5;
    if i==3 || 1
%         plot(((p(1)+1.5)*cell+xorigin)*metric,((p(2)+1.5)*cell+yorigin)*metric,'s','LineWidth',1.5)
%         plot(((q(1)+1.5)*cell+xorigin)*metric,((q(2)+1.5)*cell+yorigin)*metric,'s','LineWidth',1.5)
        %provjera vidljivosti
if 1
a1=p;
a2=q;
brpom=1;
orientation=atan2((a2(2)-a1(2)),(a2(1)-a1(1)));
pointx=round(a1(1));
pointy=round(a1(2));
rpointx=((pointx+1.5)*cell+Map_Home_x)+brpom*korak*cell*cos(orientation);
rpointy=((pointy+1.5)*cell+Map_Home_y)+brpom*korak*cell*sin(orientation);
% plot(rpointx*metric,rpointy*metric,'yo');
cpointx=max(0,floor((rpointx-cell-Map_Home_x)/cell));
cpointy=max(0,floor((rpointy-cell-Map_Home_y)/cell));
% plot(((cpointx+1.5)*cell+xorigin)*metric,((cpointy+1.5)*cell+yorigin)*metric,'c*','LineWidth',1.5)
m=[min((pointx+1),sizex) min((pointy+1),sizey)];
pointx=cpointx;
pointy=cpointy;
n=[min((pointx+1),sizex) min((pointy+1),sizey)];
wmn=korak*max(WH_dstar_cost_map(m(1),m(2)),WH_dstar_cost_map(n(1),n(2)));
% wmn=korak;


% brpom=0;
brojvecih=0;
pomocnaduljina=wmn;
while ((abs(pointx-a2(1))>1) || (abs(pointy-a2(2))>1))

        brpom=brpom+1;

    pointx=a1(1);
pointy=a1(2);
rpointx=((pointx+1.5)*cell+Map_Home_x)+brpom*korak*cell*cos(orientation);
rpointy=((pointy+1.5)*cell+Map_Home_y)+brpom*korak*cell*sin(orientation);
% plot(rpointx*metric,rpointy*metric,'yo');
cpointx=max(0,floor((rpointx-cell-Map_Home_x)/cell));
cpointy=max(0,floor((rpointy-cell-Map_Home_y)/cell));
% plot(((cpointx+1.5)*cell+xorigin)*metric,((cpointy+1.5)*cell+yorigin)*metric,'c*','LineWidth',1.5)

pointx=cpointx;
pointy=cpointy;
% cijena=WH_dstar_f_cost_map(pointx+1,pointy+1)
m=n;
n=[min((pointx+1),sizex) min((pointy+1),sizey)];
wmn=korak*max(WH_dstar_cost_map(m(1),m(2)),WH_dstar_cost_map(n(1),n(2)));
% wmn=korak;
pomocnaduljina=pomocnaduljina+wmn;

end
%zadnji ficlek koji je u istom polju
rpointa2x=((a2(1)+1.5)*cell+Map_Home_x);
rpointa2y=((a2(2)+1.5)*cell+Map_Home_y);
wmn=sqrt((rpointa2x-rpointx)^2+(rpointa2y-rpointy)^2)/100*WH_dstar_cost_map(n(1),n(2));
% wmn=sqrt((rpointa2x-rpointx)^2+(rpointa2y-rpointy)^2)/100;
pomocnaduljina=pomocnaduljina+wmn;
% provjeraduljine=sqrt((a2(2)-a1(2))^2+(a2(1)-a1(1))^2);
% return


end %od provjere vidljivosti
    end %od if i==3
            dodatak=pomocnaduljina*0.1;
    end

        suma=suma+dodatak;
    end
disp('DD* duljina puta (cijena u slucaju costmaske)')
suma
suma=0;
% for i=2:length(Dstar_path_x)
%     suma=suma+sqrt((Dstar_path_x(i)-Dstar_path_x(i-1))^2+(Dstar_path_y(i)-Dstar_path_y(i-1))^2)*0.1;
% end
for i=2:length(proslaD_x)
    dodatak=sqrt((proslaD_x(i)-proslaD_x(i-1))^2+(proslaD_y(i)-proslaD_y(i-1))^2)*0.1;
    if cijena_prepreke>2
    p(1)=proslaD_x(i-1)-0.5;%integeri u matrici
    p(2)=proslaD_y(i-1)-0.5;
    q(1)=proslaD_x(i)-0.5;
    q(2)=proslaD_y(i)-0.5;
    if i==3 || 1
%         plot(((p(1)+1.5)*cell+xorigin)*metric,((p(2)+1.5)*cell+yorigin)*metric,'s','LineWidth',1.5)
%         plot(((q(1)+1.5)*cell+xorigin)*metric,((q(2)+1.5)*cell+yorigin)*metric,'s','LineWidth',1.5)

m=round([p(1)+1 p(2)+1]);
n=round([q(1)+1 q(2)+1]);
wmn=dodatak*max(WH_dstar_cost_map(m(1),m(2)),WH_dstar_cost_map(n(1),n(2)));
% wmn=dodatak;

    end %od if i==3
            dodatak=wmn;
    end

        suma=suma+dodatak;
end
disp('D* duljina puta (cijena u slucaju costmaske)')
suma
suma=0;
if (prviput==0)
for i=2:length(trenutna_x)
    suma=suma+sqrt((trenutna_x(i)-trenutna_x(i-1))^2+(trenutna_y(i)-trenutna_y(i-1))^2)*0.1;
end
suma=0;
for i=2:length(proslaD_x)
%     WH_dstar_h_cost_map(proslaD_x(i)+0.5,proslaD_y(i)+0.5)
    suma=suma+sqrt((proslaD_x(i)-proslaD_x(i-1))^2+(proslaD_y(i)-proslaD_y(i-1))^2)*0.1;
end
end
end

if 0
    %svi pointeri
    WH_dstar_x_reverse=(load('wh_dstar_x_reverse.dat')-Map_Home_x)/cell+1;
    WH_dstar_y_reverse=(load('wh_dstar_y_reverse.dat')-Map_Home_y)/cell+1;
    WH_dstar_dx_reverse=load('wh_dstar_dx_reverse.dat');
    WH_dstar_dy_reverse=load('wh_dstar_dy_reverse.dat');

    quiver(WH_dstar_x_reverse*metric,WH_dstar_y_reverse*metric,WH_dstar_dx_reverse*metric,WH_dstar_dy_reverse*metric,0.2,'Color',[0.6 0.5 0.7]);
end

if 0    
    %svi pointeri
    WH_dstar_x=(load('wh_dstar_x.dat')+cell);
    WH_dstar_y=(load('wh_dstar_y.dat')+cell);
    WH_dstar_dx=load('wh_dstar_dx.dat');
    WH_dstar_dy=load('wh_dstar_dy.dat');

    quiver(WH_dstar_x*metric,WH_dstar_y*metric,WH_dstar_dx*metric,WH_dstar_dy*metric,0.2,'Color',[0.6 0.5 0.7]);
end
%ovo je pravi cilj patak
% goal(1)=WH_planner_globalna_putanja_x(end)-0.5;
% goal(2)=WH_planner_globalna_putanja_y(end)-0.5;

%proba
% goal=[519 131];
% goal=[171 80];
% start=[7 127];

% plot(((goal(1)+1.5)*cell+xorigin)*metric,((goal(2)+1.5)*cell+yorigin)*metric,'ys','LineWidth',1.5)
% plot(((start(1)+1.5)*cell+xorigin)*metric,((start(2)+1.5)*cell+yorigin)*metric,'ks','LineWidth',1.5)

if (0) %gledanje prepreka od lasera i staticke prepreke
M_laser_obstacles=load('log_laser_obstacles.dat'); %prepreke [x1 y1;x2 y2;x3 y3;...]
  plot(M_laser_obstacles(:,1)*metric+0.1,M_laser_obstacles(:,2)*metric+0.1,'c*');
moving_obstacle_x=load('wh_gridmap_moving_x.dat');
moving_obstacle_y=load('wh_gridmap_moving_y.dat');
  plot(moving_obstacle_x*metric+0.1,moving_obstacle_y*metric+0.1,'m*');

  for i=1:sizex
    for j=1:sizey
        if gmap(i,j)>0 && gmappol(i,j)==0
            goal=[i j];
            plot(((goal(1)+0.5)*cell+xorigin)*metric,((goal(2)+0.5)*cell+yorigin)*metric,'gs','LineWidth',1.5)
        end
    end
  end

  for i=1:sizex
    for j=1:sizey
        if gmap(i,j)>0 && gmappol(i,j)>0
            goal=[i j];
            plot(((goal(1)+0.5)*cell+xorigin)*metric,((goal(2)+0.5)*cell+yorigin)*metric,'bs','LineWidth',1.5)
        end
    end
  end
end
  
%provjera vidljivosti
if 0
a1=[trenutna_x(1) trenutna_y(1)]-0.5;
a2=[trenutna_x(2) trenutna_y(2)]-0.5;

orientation=atan2((a2(2)-a1(2)),(a2(1)-a1(1)));
pointx=a1(1);
pointy=a1(2);
rpointx=((pointx+1.5)*cell+Map_Home_x)+brpom*0.5*cell*cos(orientation);
rpointy=((pointy+1.5)*cell+Map_Home_y)+brpom*0.5*cell*sin(orientation);
plot(rpointx*metric,rpointy*metric,'yo');
cpointx=floor((rpointx-cell-Map_Home_x)/cell);
cpointy=floor((rpointy-cell-Map_Home_y)/cell);
plot(((cpointx+1.5)*cell+xorigin)*metric,((cpointy+1.5)*cell+yorigin)*metric,'c*','LineWidth',1.5)

pointx=cpointx;
pointy=cpointy;

brpom=0;
brojvecih=0;
while ((abs(pointx-a2(1))>1) || (abs(pointy-a2(2))>1))

        brpom=brpom+1;

    pointx=a1(1);
pointy=a1(2);
rpointx=((pointx+1.5)*cell+Map_Home_x)+brpom*0.5*cell*cos(orientation);
rpointy=((pointy+1.5)*cell+Map_Home_y)+brpom*0.5*cell*sin(orientation);
plot(rpointx*metric,rpointy*metric,'yo');
cpointx=floor((rpointx-cell-Map_Home_x)/cell);
cpointy=floor((rpointy-cell-Map_Home_y)/cell);
plot(((cpointx+1.5)*cell+xorigin)*metric,((cpointy+1.5)*cell+yorigin)*metric,'c*','LineWidth',1.5)

pointx=cpointx;
pointy=cpointy;

    if (WH_dstar_f_cost_map(pointx+1,pointy+1)~=minf)

								 brojvecih=brojvecih+1;
								 break;
    end


end

end %od provjere vidljivosti

grid off
view([0 90]);
% view([-90 90]);

% naziv=strcat('funkcija f (min(f)=',num2str(minf),', max(f)=',num2str(maxf),')')
% title(naziv,'fontsize',16);

    ylabel('y [m]', 'fontsize',16,'fontname', 'times');
    xlabel('x [m]', 'fontsize',16,'fontname', 'times');
    h=gca;
    set(h,'fontsize',12,'fontname','times','box', 'on');

axis equal tight
xmin=xmin*cell+xorigin;
xmax=xmax*cell+xorigin;
ymin=ymin*cell+yorigin;
ymax=ymax*cell+yorigin;
axis([xmin xmax ymin ymax]*metric)
% axis([-28 1 -4.5 7.5])   %zavod
%  axis([-24.5 -20.5 -23.5 -22.2])   %ppt icat
%  axis([-24.6 -20.5 -23.5 -22.1])   %clanak syroco robot representation

