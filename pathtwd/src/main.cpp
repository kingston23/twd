#include <sys/time.h>
#include <iostream>
#include <string>
#include "ros/ros.h"
//#include <algorithm>

//#include <twdplanner/Point.h>
#include <twdplanner/Path.h>
#include <visualization_msgs/Marker.h>


class VisualizationPublisherTWD
{
protected:
  // Our NodeHandle
  ros::NodeHandle nh_;
  std::string target_frame_;

public:
	ros::Publisher pathvis_pub;
	ros::Subscriber pathtwd_sub;


    visualization_msgs::Marker pathvis;

  VisualizationPublisherTWD(ros::NodeHandle n) :
      nh_(n),  target_frame_("odom") 
  {

 	pathtwd_sub = nh_.subscribe("twdPath",1,&VisualizationPublisherTWD::pathCallback, this);
	pathvis_pub = nh_.advertise<visualization_msgs::Marker>("/path_markerListener",10);

    pathvis.header.frame_id = target_frame_;
    pathvis.header.stamp = ros::Time::now();
    pathvis.ns =  "pathtwd";
    pathvis.action = visualization_msgs::Marker::ADD;
    pathvis.pose.orientation.w  = 1.0;
    pathvis.type = visualization_msgs::Marker::LINE_STRIP;
    pathvis.scale.x = 0.05; 
    pathvis.scale.y = 0.05; 
    pathvis.color.r = 0.6;
    pathvis.color.g = 0.;
    pathvis.color.b = 0.8;
    pathvis.color.a = 1.0;
      

  }

  void visualizationduringmotion();
  void pathCallback(const twdplanner::PathConstPtr& pMsg);

};



int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "pathtwd");
  ros::NodeHandle nh;
  VisualizationPublisherTWD visualTWD(nh);


  ros::Rate rate(10.0);


  while (nh.ok()) {


    ros::spinOnce(); 

	  visualTWD.visualizationduringmotion();	
    
    
	  rate.sleep();
  }
  return 0;
}


void VisualizationPublisherTWD::visualizationduringmotion(){

			pathvis_pub.publish(pathvis);

}

void VisualizationPublisherTWD::pathCallback(const twdplanner::PathConstPtr& pMsg)
{
  
  pathvis.points.clear();
  ROS_INFO("CISTIMO SVEEEEEE");

  geometry_msgs::Point p; 
  ROS_INFO("Velicina pMsg->points %d: ", pMsg->points.size());

	for (int i=0; i<pMsg->points.size(); i++){
		p.x=pMsg->points[i].x;
		p.y=pMsg->points[i].y;
    ROS_INFO("Tocka na putu %d: [%f %f]", i, p.x, p.y);

		pathvis.points.push_back(p);
	}


}



