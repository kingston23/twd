cmake_minimum_required(VERSION 2.4.6)
project(pathtwd)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++0x")
#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

find_package(twdplanner REQUIRED)
find_package(movingobstaclesrhc REQUIRED)
find_package(cspacevoroandrea REQUIRED)

find_package(catkin REQUIRED COMPONENTS
  std_msgs
  nav_msgs
  geometry_msgs
  message_generation
  roscpp
  tf
#  nodelet
)

## Generate messages in the 'msg' folder
add_message_files(
   FILES
   FilteredPath.msg
   FixOrientation.msg
   patrolingGoals.msg
   Point.msg
   FilteredTWDPath.msg
)

generate_messages(
   DEPENDENCIES
   std_msgs
   nav_msgs
   geometry_msgs
)

catkin_package(
  CATKIN_DEPENDS std_msgs message_runtime
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${twdplanner_INCLUDE_DIRS}
  ${movingobstaclesrhc_LIBRARIES}
  ${cspacevoroandrea_LIBRARIES}
  include
)


add_executable(path src/main.cpp)

#target_link_libraries(mapls ${maptogridmap_LIBRARIES} ${catkin_LIBRARIES})
target_link_libraries(path ${catkin_LIBRARIES} ${twdplanner_LIBRARIES} ${movingobstaclesrhc_LIBRARIES} ${cspacevoroandrea_LIBRARIES})
add_dependencies(path pathtwd_generate_messages_cpp)





